# React Redux with React-Router-Redux Stocks and News
- Webpack 3.11.0
- Redux
- Redux Pesist using LocalForage(underthe hood IndexDB, being the Async and better solution compared to localStorage)
- Routing using React-Router-Dom
- Bootstrap v4 with Reactstrap
- Separate Dev and Prod environment setup
- redux-devtools on screen in Dev mode
- Stylus CSS library with CSS modules configuration
- Configured Manta's Stylus Supremacy to format stylus files properly(default settings formats too much). Project must be open in VS Code so that .vscode config folder is at root of path.
- Centralized system for colors in styles/colors.styl, In Stylus: @import '../../styles/colors' In Javascript: import colors from '../../styles/colors.styl';
- Global styles in styles/styles.styl, although still need to be imported due to CSS Module approach
- Parallax Effect on background image
- Axios library to fetch requests
- Uses OpenBrowserPlugin to open page in browser upon startup
- Uses File Loader to use images in assets folder
- Hosted via firebase.

# Contains
- Fetches news on a stock from Webhose, Guardian, and NewsApi
- Fetches stock data and display in a stock chart, using AlphaVantage Api, and React-Stockcharts library
- Firebase Database example
- Firebase Firestore example
- Account and Login pages using firebase
- Data Tables using react-bootstrap-table-2(react-bootstrap-table-next)

# Programming concepts purposefully ommited:
- Typescript seems like a popular approach in programming, but doesn't provide anything to the table beyond declarative types for javascipt, yet makes the development cycles and code harder to manage to support its pattern, and a hefty library size and added wait time to compilation(although you could use ahead of time compilation to speed it up). For the newest javascipt(ES(insert_each_year_since_2015_here)), there's Babel for that.
- Separate action types for Redux, having separate files for each dispatch call would make for uneccessary cluster in the code for the given application design.

# To Run:

Unzip files/Github clone.

Make sure you have 'npm' and 'yarn' installed.

Navigate to the folder level where package.json is in console and run 'npm install' or 'yarn install' while will download node_modules.

After 'npm install'/'yarn install' finishes running, run in the same place 'npm start' or 'npm run dev'. A webpage should auto open in browser.

# Useful Pluggins for VS Code : 'Auto Close Tag', 'Auto Import', 'TODO Highlight', 'language-stylus', 'Manta's Stylus Supremacy'(format's stylus files)

# If you prefer SCSS over Stylus:
- yarn add node-sass
- yarn add sass-loader
- get rid of stylus-loader in package.json or remove it through command line
- change webpack files, prod and dev to recognize sass/scss
- change the ending of your styling files from '.styl' across the project

# Useful VS Code keyboard shortcuts to know: 
- Indent-left/Indent Right: 'Ctrl + [' and 'ctrl + ]' on selected selected lines.
- Format Document: Windows-> 'Shift + Alt + F'. Max-> 'Shift + Option + F'. Ubuntu-> 'Ctrl + Shift + I'.
- Multi Line comments: Windows-> 'Ctrl + /'. Mac-> 'Command ⌘ + /'.  on selected lines. Multiple line comments are easier to uncomment sections of with keyboard commands then using '/*', '*/' syntax.

# Notes:
- LocalForage uses indexDB/WebSQL which can still be used by other's, so instead of storing email and password, my recomendation is to store a token in LocalForage that exists on the database, which acts as a pointer towards sensitive information like email and password neccessary to login into the app upon launch. For extra security, if possible also store the machine identifier so that the token can't be used on another machine. Also make the token have a lifetime, giving it a lifetime will ensure that long forgotten token's don't work and pose a threat to security, but also to clean up the database from old tokens continuing to take up space. Note that firebase has a built in token system, don't need to re-invent the wheel.
- If you added a library using npm, remember to do 'yarn install' so that yarn will recognize the added library as well, or simply use yarn add 'library name' instead of npm, otherwise there might be an error upon launching the project.
- When in development mode, on windows press 'CTRL + H' to toggle the right hand redux dev tool.

# Publishing:
- Change src="/bundle.js" to src="/dist/bundle.js" in index.html
- Run 'yarn run build'
- Run 'yarn run deploy' or 'firebase deploy'

# TODO: