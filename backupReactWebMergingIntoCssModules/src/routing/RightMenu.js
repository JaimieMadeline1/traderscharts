import React, { Component } from 'react';
import ReactDOM from 'react-dom';

// Routing
import { Switch, Route } from "react-router-dom";
import { Router, IndexRedirect, browserHistory } from 'react-router';

// Import Screens(high level components)
import Stocks from '../screens/Stocks/Stocks';
import NotFound from '../screens/NotFound';
import FirebaseDatabase from '../screens/FirebaseDatabase';
import FirebaseFirestore from '../screens/FirebaseFirestore';
import DisplayTables from '../screens/DisplayTables/DisplayTables';
import Account from '../screens/Account/Account';
import LogInAndRegister from '../screens/LogInAndRegister/LogInAndRegister';
import DescriptionPage from '../screens/DescriptionPage/DescriptionPage';

class LeftMenu extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <React.Fragment>
                <Switch>
                    <Route exact path="/" component={DescriptionPage} />
                    <Route path={'/firebasedatabase'} component={FirebaseDatabase} />
                    <Route path={'/firebasefirestore'} component={FirebaseFirestore} />
                    <Route path={'/displaytables'} component={DisplayTables} />
                    <Route path={'/account'} component={Account} />
                    <Route path={'/login'} component={LogInAndRegister} />
                    <Route path={'/stocks'} component={Stocks} />
                    <Route component={NotFound} />
                </Switch>
            </React.Fragment>
        );
    }
}

export default LeftMenu;
