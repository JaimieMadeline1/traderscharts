import React, { Component } from 'react';
import ReactDOM from 'react-dom';
// import styles from '../styles/styles.styl';
import colors from '../styles/colors.styl';
import { ClipLoader } from 'react-spinners';
import { connect } from 'react-redux';

class Global extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <React.Fragment>
                <div className="sweetLoading">
                    <ClipLoader
                        color={colors.spinner}
                        size={32}
                        loading={this.props.showSpinner ? this.props.showSpinner : false}
                    />
                </div>
            </React.Fragment >
        );
    }
}

const mapStateToProps = (state) => {
    return {
        showSpinner: state.loading.isLoading,
    }
}

export default connect(mapStateToProps)(Global);
