import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import styles from './Account.styl';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import { connect } from 'react-redux';
import {
    AUTH_LOGOUT,
    USER_RESET,
    LOADING_ON,
    LOADING_OFF
} from '../../constants/actionTypes';

// Opened by pressing on the header icon/tag of the user logged in.
class Account extends React.Component {
    constructor(props) {
        super(props);
        this.state = { status: '', name: this.props.name, email: this.props.email, tempName: this.props.name, tempEmail: this.props.email };
    }
    logOut() {
        this.props.loadingOn();
        this.props.logOut();
        this.props.resetUserState();
        this.props.loadingOff();
        this.props.history.push('/');
    }
    resetPassword() {

    }
    updateInfo() {

    }
    render() {
        return (
            <React.Fragment>
                <div className={[styles.mainContainer].join(' ')}>
                    <h2 className={styles.title}>Account</h2>
                    <h5>Reset Password and updating info don't work at the moment.</h5>
                    <Form>
                        {/* <h4 className={styles.status}>{this.state.status}</h4> */}

                        {/* <div className={styles.eachInputSection}> */}
                        <FormGroup>
                            <Label className={styles.inputHeaderText}>Name</Label>
                            <Input
                                value={this.state.tempName}
                                placeholder='Enter name of new user'
                                onChange={(e) => this.setState({ tempName: e.target.value })}
                                type="text"
                            />
                        </FormGroup>
                        {/* </div> */}

                        <div className={styles.eachInputSection}>
                            <Label className={styles.inputHeaderText}>Email</Label>
                            <Input
                                value={this.state.tempEmail}
                                placeholder='Enter name of new user'
                                onChange={(e) => this.setState({ tempEmail: e.target.value })}
                                type="text"
                            />
                        </div>
                        {/* {this.state.isChanged == true &&
                            <div>
                                <button
                                    className={styles.buttonStyle}
                                    onClick={() => { this.updateInfo.bind(this) }}>
                                    Update Info
                                    </button>
                            </div>
                        } */}
                        <div>
                            <div className={["container", styles.buttonContainer].join(' ')}>
                                <div className="row">
                                    <div className={'col-sm-2'}></div>
                                    <Button className='col-sm-3' outline color="primary" style={{ whiteSpace: 'normal' }}
                                        onClick={() => { this.resetPassword() }}>
                                        Password Reset
                                    </Button>
                                    <div className={'col-sm-2'}></div>
                                    <Button className='col-sm-3' outline color="primary"
                                        onClick={() => { this.logOut() }}>
                                        Log Out
                                    </Button>
                                    <div className={'col-sm-2'}></div>
                                </div>
                            </div>
                        </div>
                    </Form>
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        name: state.userState.userName,
        email: state.userState.userEmail
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        logOut: () => dispatch({ type: AUTH_LOGOUT }),
        resetUserState: () => dispatch({ type: USER_RESET }),
        loadingOn: () => dispatch({ type: LOADING_ON }),
        loadingOff: () => dispatch({ type: LOADING_OFF })
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Account);
