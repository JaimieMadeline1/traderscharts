import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { ClipLoader } from 'react-spinners';

import HomeStyles from './Home.styl';
import colors from '../../styles/colors.styl';
import styles from '../../styles/styles.styl';
import globalStyles2 from '../../styles/globalStyles2.styl';

import Global from '../Global';
import Header from '../Header/Header';
import Body from '../Body/Body';
import Footer from '../Footer';

class Home extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <React.Fragment>
                <Global></Global>
                <Header></Header>
                <div className={styles.mainStyle}>
                    <Body></Body>
                </div>
                <Footer></Footer>
            </React.Fragment >
        );
    }
}

export default Home;
