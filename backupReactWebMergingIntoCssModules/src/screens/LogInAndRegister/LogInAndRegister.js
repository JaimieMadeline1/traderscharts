import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import styles from './LogInAndRegister.styl';
import colors from '../../styles/colors.styl';
import { Button, Form, FormGroup, Label, Input, FormText, Alert } from 'reactstrap';

import { connect } from 'react-redux';
import {
    AUTH_LOGIN,
    USER_ADD_USER,
    LOADING_ON,
    LOADING_OFF
} from '../../constants/actionTypes';

import firebase from 'firebase';
// Required for side-effects
require("firebase/firestore");

var db = firebase.firestore();

class LogInAndRegister extends React.Component {
    constructor(props) {
        super(props);
        this.state = { email: 'Vlad2@gmail.com', password: 'Password1', name: '', isLoginMode: true, status: '' };
    }
    onLogIn() {
        // this.setState({ status: '', loading: true, showSpinner: true });
        this.props.loadingOn();
        const { email, password, name } = this.state;
        if (password != '' && email != '') {
            firebase.auth().signInWithEmailAndPassword(email, password)
                .then((user) => {
                    db.collection("users").doc(user.uid).get()
                        .then((doc) => {
                            this.props.AuthLogin(email, password, doc.data().name);
                            this.props.AddUser(doc.data().name, email, doc.id);
                            this.props.loadingOff();
                            // Transfer out of this screen to home page.
                            this.props.history.push('/');
                        }).catch((error) => {
                            this.setState({ status: error.message });
                            this.props.loadingOff();
                        })
                })
                .catch((error) => {
                    // alert(error.message);
                    this.setState({ status: error.message });
                    this.props.loadingOff();
                })
        } else {
            this.setState({ status: 'Empty Email or Password' });
            this.props.loadingOff();
        }
    }
    onRegister() {
        this.props.loadingOn();
        // this.setState({ status: '', loading: true, showSpinner: true });
        const { email, password, name } = this.state;
        if (password != '' && email != '') {
            firebase.auth().createUserWithEmailAndPassword(email, password)
                .then((user) => {
                    db.collection("users").doc(user.uid).set({
                        name: name,
                        email: email
                    })
                        .then((userInDB) => {
                            this.props.AuthLogin(email, password, name);
                            this.props.AddUser(name, email, userInDB.id);
                            this.props.loadingOff();
                            // this.props.AddUser(name, email, user.user.uid);
                            this.props.history.push('/')
                            // Make a new token of the user's device, and send to database for push notifications.
                            // this.sendTokenAndRegister(user.user.uid);
                        })
                        .catch((error) => {
                            this.setState({ status: error.message });
                            this.props.loadingOff();
                        })

                })
                .catch((error) => {
                    // alert(error.message);
                    this.setState({ status: error.message });
                    this.props.loadingOff();
                })
        } else {
            this.setState({ status: 'Empty Email or Password' });
            this.props.loadingOff();
        }
    }
    changeMode() {
        this.setState((prevState, props) => {
            return { isLoginMode: !prevState.isLoginMode, status: '' };
        });
    }
    render() {
        return (
            <React.Fragment>
                <div className={[styles.mainContainer, 'form-group'].join(' ')}>
                    <h2 className={styles.title}>{this.state.isLoginMode == true ? 'Login' : 'Registeration'}</h2>
                    <div className={styles.eachInputSection}>
                        <Label className={styles.inputHeaderText}>Enter Email</Label>
                        <Input
                            className={'form-control'}
                            value={this.state.email}
                            placeholder='Enter name of new user'
                            onChange={(e) => this.setState({ email: e.target.value })}
                            type="text"
                        />
                    </div>
                    <div className={styles.eachInputSection}>
                        <Label className={styles.inputHeaderText}>Enter Password</Label>
                        <Input
                            className={'form-control'}
                            value={this.state.password}
                            placeholder='Enter name of new user'
                            onChange={(e) => this.setState({ password: e.target.value })}
                            type="text"
                        />
                    </div>
                    {this.state.isLoginMode == false &&
                        <div className={styles.eachInputSection}>
                            <Label className={styles.inputHeaderText}>Enter Name(Optional)</Label>
                            <Input
                                className={'form-control'}
                                value={this.state.name}
                                placeholder='Enter name of new user'
                                onChange={(e) => this.setState({ name: e.target.value })}
                                type="text"
                            />
                        </div>
                    }
                    <div>
                        <div className={["container", styles.buttonContainer].join(' ')}>
                            <div className="row">
                                <div className={'col-sm-1'}></div>
                                <Button
                                    className={'col-sm-4'} outline color="primary" style={{ whiteSpace: 'normal' }}
                                    onClick={() => {
                                        this.state.isLoginMode == true ? this.onLogIn() : this.onRegister()
                                    }}>
                                    {this.state.isLoginMode == true ? 'Login' : 'Create Account'}
                                </Button>
                                <div className={'col-sm-2'}></div>
                                <Button
                                    className={'col-sm-4'} outline color="primary" style={{ whiteSpace: 'normal' }}
                                    onClick={() => { this.changeMode() }}>
                                    Change to {this.state.isLoginMode == false ? 'Login' : 'Registeration'}
                                </Button>
                                <div className={'col-sm-1'}></div>
                            </div>
                        </div>
                    </div>
                    {this.state.status &&
                        <Alert className={styles.status} color="danger">
                            {this.state.status}
                        </Alert>
                        // <Label className={["text-danger", 'error', styles.status].join(' ')}>{this.state.status}</Label>
                    }
                </div>
            </React.Fragment >
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        AuthLogin: (email, password, name) => dispatch({ type: AUTH_LOGIN, email, password, name }),
        AddUser: (name, email, userUid) => dispatch({ type: USER_ADD_USER, name, email, userUid }),
        loadingOn: () => dispatch({ type: LOADING_ON }),
        loadingOff: () => dispatch({ type: LOADING_OFF })
    }
}

export default connect(null, mapDispatchToProps)(LogInAndRegister);
