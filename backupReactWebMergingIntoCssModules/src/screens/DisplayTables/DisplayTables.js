import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import colors from '../../styles/colors.styl';
import styles from './DisplayTables.styl';

import {
    AUTH_LOGIN,
    AUTH_LOGOUT
} from '../../constants/actionTypes';

import BootstrapTable from 'react-bootstrap-table-next';
// import paginationFactory from 'react-bootstrap-table2-paginator';

const products = [
    { id: '1', name: 'Flute', price: '100' },
    { id: '2', name: 'Piano', price: '1000' },
    { id: '3', name: 'Cello', price: '10000000' },
    { id: '4', name: 'Violin', price: '1000000000' }];

const columns = [{
    dataField: 'id',
    text: 'Product ID',
    sort: true
}, {
    dataField: 'name',
    text: 'Product Name',
    sort: true
}, {
    dataField: 'price',
    text: 'Product Price',
    sort: true
}];

const selectRow = {
    mode: 'checkbox',
    style: { background: colors.tableCheckBoxColor, color: colors.tableCheckActiveTextColor }
};

class DisplayTables extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <React.Fragment>
                {/* <div className="mainContainerTablePage"> */}
                <div className={styles.mainContainerTablePage}>
                    <BootstrapTable selectRow={selectRow} striped hover bordered keyField='id' data={products} columns={columns} />
                </div>
            </React.Fragment >
        );
    }
}

export default DisplayTables;
