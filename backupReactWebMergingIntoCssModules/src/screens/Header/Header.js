import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import styles from './Header.styl';
// import RNFirebaseLogo from '../../assets/RNFirebase.png';
const logo = require('../../assets/RNFirebase.png');
import Account from '../Account/Account';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';

import { Link, withRouter } from "react-router-dom";
// import { browserHistory } from 'react-router'
// import createHistory from 'history/createBrowserHistory';

// Create a history of your choosing (we're using a browser history in this case)
// const history = createHistory();

const appConstants = require('../../constants/appConstants');
import { connect } from 'react-redux';
// import {
//     AUTH_PRELOG
// } from '../../constants/actionTypes';

class Header extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <React.Fragment>
                <div className={styles.heading}>
                    <div className={["container", styles.container].join(' ')}>
                        <div className="row">
                            <div className={["col-lg-2", "col-md-2", "col-xs-12"].join(' ')}>
                                <img width="50" src={logo} alt="logo" />
                            </div>
                            <h2 className={[styles.headerText, "col-lg-7", "col-md-10", "col-xs-12"].join(' ')}>{appConstants.APP_NAME}</h2>
                            {/* <button className={[styles.logInButton, "col-lg-2"].join(' ')}
                                onClick={this.changeLogInState()}>{this.props.isloggedIn == false ? 'Log In' : 'Log Out'}</button> */}
                            <div className="col-lg-1"></div>
                            {this.props.isloggedIn == false ?
                                <Link to="./login" className={["col-lg-2", "col-xs-12", styles.loginProfile].join(' ')}>
                                    <Button color="primary" size="lg" style={{ whiteSpace: 'normal' }}
                                        onClick={() => { }}>Log In</Button>
                                </Link>
                                // <Button outline color="primary"
                                //     onClick={() => { this.logOut() }}>
                                //     Log Out
                                // </Button>
                                :
                                // <div className="col-lg-2">
                                <Link className={['col-lg-2', "col-xs-12", styles.loginProfile].join(' ')} to="./account">
                                    {/* <Label onClick={() => { }}> */}
                                    <div className={styles.logInText}>Logged in as: {this.props.userStateName != '' ? this.props.userStateName : 'No name given.'}</div>
                                    {/* </Label> */}
                                </Link>
                                /* <button className={[styles.logInButton, "col-lg-2"].join(' ')}
                        onClick={() => { this.logOut() }}>Log Out</button> */
                                // </div>
                            }
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        isloggedIn: state.auth.isLoggedIn,
        userStateName: state.userState.userName,
    }
}

export default withRouter(connect(mapStateToProps)(Header));
