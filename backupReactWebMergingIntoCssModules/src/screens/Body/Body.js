import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import styles from './Body.styl';
import { connect } from 'react-redux';

import LeftMenu from '../../routing/LeftMenu';
import RightMenu from '../../routing/RightMenu';

class Body extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <React.Fragment>
                    <div className={styles.containerOverBoxes}>
                        <div className="container">
                            <div className="row">
                                <div className={['col-xl-4', styles.leftMenuPadding].join(' ')}>
                                    <div className={[styles.leftMain, styles.main].join(' ')}>
                                        <LeftMenu></LeftMenu>
                                    </div>
                                </div>
                                <div className="col-xl-1"></div>
                                <div className={[styles.rightMain, styles.main, "col-xl-7"].join(' ')}>
                                    <RightMenu></RightMenu>
                                </div>
                            </div>
                        </div>
                    </div>
            </React.Fragment>
        );
    }
}

export default Body;