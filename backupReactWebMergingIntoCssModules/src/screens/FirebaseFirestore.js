import React, { Component, Image } from 'react';
import ReactDOM from 'react-dom';
import firebase from 'firebase';
// Required for side-effects
require("firebase/firestore");

import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';

// const FIREBASE_CONFIG = {
//     apiKey: "AIzaSyBNwKakeSJ2pJPufMH7jvSEK-0Cq70I6EU",
//     authDomain: "caterpalacetest.firebaseapp.com",
//     databaseURL: "https://caterpalacetest.firebaseio.com",
//     projectId: "caterpalacetest",
//     storageBucket: "gs://caterpalacetest.appspot.com",
//     messagingSenderId: "536293245368"
// };

// firebase.initializeApp(FIREBASE_CONFIG);

var db = firebase.firestore();

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {
    LOADING_ON,
    LOADING_OFF
} from '../constants/actionTypes';

class FirebaseFirestore extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            listOfClients: [],
            name: '',
            chosenImage: ''
        };
        this.dataBackup = [];
    }
    componentDidMount() {
        this.props.loadingOn();

        db.collection("users").get().then((querySnapshot) => {
            let newData = [];
            querySnapshot.forEach((doc) => {
                // console.log(`${doc.id} => ${doc.data()}`);
                let name = doc.data().name;
                let initials = name.match(/\b\w/g) || [];
                initials = ((initials.shift() || '') + (initials.pop() || '')).toUpperCase();

                newData.push({
                    uid: doc.id,
                    name: name,
                    portfolioUri: '',
                    nickname: initials
                })

                this.dataBackup = newData;
                this.loadImages();
                // this.setState({ listOfClients: newData });
            });
        });
    }
    componentWillUnmount() {
        this.props.loadingOff();
    }
    loadImages() {
        Promise.all(this.dataBackup.map((item) => {
            const rootRefStorage = firebase.storage().ref('NewData');
            const userRefStorage = rootRefStorage.child(item.uid);
            const profilePicRefStorage = userRefStorage.child('ProfilePictures');
            const imageRefStorage = profilePicRefStorage.child('profileImg');
            return imageRefStorage.getDownloadURL()
                .then((url) => {
                    item.portfolioUri = url;
                })
                .catch((error) => {
                    // this.setState({ status: error.message });
                });
        })
        )
            .then(() => {
                this.props.loadingOff();
                this.loadImagesFinalDestination();
            })
            .catch(() => {
                this.props.loadingOff();
            });
    }
    loadImagesFinalDestination() {
        this.setState({ listOfClients: this.dataBackup });
    }
    uploadImageToStorage(uri, imageName, uid, mime = 'image/jpg') {
        // return new Promise((resolve, reject) => {
        // const uploadUri = Platform.OS === 'ios' ? uri.replace('file://', '') : uri;
        // // Save the collected image as the 
        const rootRef = firebase.storage().ref('NewData');
        const userRef = rootRef.child(uid);
        const profilePicRef = userRef.child('ProfilePictures');
        const imageRef = profilePicRef.child(imageName);
        imageRef.put(this.dataURItoBlob(uri), { contentType: mime })
            .then(function (snapshot) {
                console.log('Uploaded a blob or file!');
            })
            .catch(() => {
                console.log('Failed Upload!');
            })
    }
    addNewUser() {
        let newName = this.state.name;
        db.collection("users").add({
            name: newName
        })
            .then((docRef) => {
                console.log("Document written with ID: ", docRef.id);

                this.uploadImageToStorage(this.state.chosenImage, 'profileImg', docRef.id);

                let tempList = this.state.listOfClients;
                let initials = newName.match(/\b\w/g) || [];
                initials = ((initials.shift() || '') + (initials.pop() || '')).toUpperCase();
                let newObj = {
                    uid: docRef.id,
                    name: newName,
                    portfolioUri: this.state.chosenImage,
                    nickname: initials
                }
                tempList.push(newObj);
                this.setState({ name: '', chosenImage: '', listOfClients: tempList });
            })
            .catch((error) => {
                console.error("Error adding document: ", error);
            });
    }
    dataURItoBlob(dataURI) {
        // convert base64/URLEncoded data component to raw binary data held in a string
        var byteString;
        if (dataURI.split(',')[0].indexOf('base64') >= 0)
            byteString = atob(dataURI.split(',')[1]);
        else
            byteString = unescape(dataURI.split(',')[1]);

        // separate out the mime component
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

        // write the bytes of the string to a typed array
        var ia = new Uint8Array(byteString.length);
        for (var i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }

        return new Blob([ia], { type: mimeString });
    }
    onReadImage(event) {
        if (event.target.files && event.target.files[0]) {
            let reader = new FileReader();
            reader.onload = (e) => {
                this.setState({ chosenImage: e.target.result });
            };
            reader.readAsDataURL(event.target.files[0]);
        }
    }
    render() {
        return (
            <React.Fragment>
                <h3>Firebase Test.</h3>
                <h4>List all the client's names from firebase database below:</h4>
                <div>
                    <div className="container">
                        <div className="row">
                            <Input
                                className='col-sm-3'
                                value={this.state.name}
                                placeholder='Enter name'
                                onChange={(e) => this.setState({ name: e.target.value })}
                                type="text"
                            />
                            <Button className='col-sm-3' outline color="primary" style={{ whiteSpace: 'normal' }}
                                // style={{ backgroundColor: 'Orange', color: 'white' }}
                                onClick={() => { this.addNewUser.bind(this) }}>
                                Add user
                            </Button>
                            {/* <div className='col-sm-3' /> */}
                        </div>
                    </div>
                    <div>
                        <Input style={{ padding: '0px' }} className='col-sm-6' id="upload" ref="upload" type="file" accept="image/*"
                            onChange={this.onReadImage.bind(this)}
                        // onClick={(event)=> { 
                        //     event.target.value = null
                        // }}
                        />
                        <img class="rounded" style={this.state.chosenImage != '' ? { width: '200px', height: '200px' } : {}}
                            src={this.state.chosenImage}
                        />
                    </div>
                </div>

                <div style={{ marginLeft: '30px' }}>
                    {this.state.listOfClients.length != 0 ?
                        <div>
                            {this.state.listOfClients.map((client) =>
                                <div style={{ border: '1px solid black', padding: '10px', margin: '10px' }} key={client.uid}>
                                    {client.portfolioUri != '' ?
                                        <img src={client.portfolioUri} style={{ width: '60px', height: '60px' }} />
                                        :
                                        <div style={{ width: '60px', height: '60px', border: '1px solid black' }}>{client.nickname}</div>
                                    }
                                    <div>Name: {client.name}</div>
                                </div>
                            )}
                        </div>
                        :
                        <div>
                            <h5>There are no clients at the moment.</h5>
                        </div>
                    }
                </div>
            </React.Fragment>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        loadingOn: () => dispatch({ type: LOADING_ON }),
        loadingOff: () => dispatch({ type: LOADING_OFF })
    }
}

export default connect(null, mapDispatchToProps)(FirebaseFirestore);
