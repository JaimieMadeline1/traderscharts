import { USER_ADD_USER, USER_CHANGE_USER_EMAIL, USER_CHANGE_USER_NAME, USER_RESET } from '../constants/actionTypes';

const initialUserState = { userUid: '', userEmail: '', userName: '' };

export const userState = (state = initialUserState, action) => {
    switch (action.type) {
        case USER_ADD_USER:
            return { ...state, userName: action.name, userEmail: action.email, userUid: action.uid };
        case USER_CHANGE_USER_NAME:
            return { ...state, userName: action.name };
        case USER_CHANGE_USER_EMAIL:
            return { ...state, userEmail: action.email };
        case USER_RESET:
            return { ...state, initialUserState };
        default:
            return state;
    }
}