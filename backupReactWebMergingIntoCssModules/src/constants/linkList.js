const linkList = [
    { id: 0, linkName: 'Home', linkTo: '/' },
    { id: 1, linkName: 'Stocks', linkTo: '/stocks' },
    { id: 2, linkName: 'Firebase Database', linkTo: '/firebasedatabase' },
    { id: 3, linkName: 'Firebase Firestore', linkTo: '/firebasefirestore' },
    { id: 4, linkName: 'Display Tables', linkTo: '/displaytables' }
];

export default linkList;