var webpack = require('webpack');
var path = require('path');

var parentDir = path.join(__dirname, '../../');
var OpenBrowserPlugin = require('open-browser-webpack-plugin');
var cssModulesPaths = require('../cssModulesPaths');

module.exports = {
    entry: [
        path.join(parentDir, 'index.js')
    ],
    output: {
        path: parentDir + '/dist',
        filename: 'bundle.js'
    },
    devServer: {
        contentBase: parentDir,
        historyApiFallback: true,
        overlay: true
    },
    module: {
        loaders: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                loader: 'babel-loader'
            },
            {
                test: /\.(png|jpg|gif)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {}
                    }
                ]
            },
            {
                test: /\.css$/,
                loader: 'style-loader!css-loader',
                include: [
                    path.resolve(parentDir, 'node_modules/bootstrap'),
                ]
            },
            {
                test: /\.styl$/,
                exclude: [
                    path.resolve(parentDir, 'node_modules'),
                ],
                include: [
                    cssModulesPaths.stylusInclude.map((item) =>
                        path.resolve(parentDir, item),
                    )
                ],
                use: [
                    'style-loader',
                    {
                        loader: 'css-loader',
                        // options: {
                        //   modules: false,
                        //   localIdentName: '[name]__[local]___[hash:base64:5]'
                        // }
                    },
                    'stylus-loader'
                ]
            },
            {
                test: /\.styl$/,
                exclude: [
                    cssModulesPaths.cssModuleExlude.map((item) =>
                        path.resolve(parentDir, item),
                    )
                    // cssModuleExlude
                    // path.resolve(parentDir, 'node_modules'),
                    // path.resolve(parentDir, 'src/styles/globalStyles2'),
                    // path.resolve(parentDir, 'src/styles/styles'),
                    // Instead of the above two lines; Make sure colors are in a separate folder; Use line below:
                    // path.resolve(parentDir, 'src/styles'),
                ],
                use: [
                    'style-loader',
                    {
                        loader: 'css-loader',
                        options: {
                            modules: true,
                            localIdentName: '[name]__[local]___[hash:base64:5]'
                        }
                    },
                    'stylus-loader'
                ],
            },
        ],
        // noParse: [new RegExp('node_modules/localforage/dist/localforage.js')]
    },
    plugins: [
        /**
         * NoErrorsPlugin prevents your webpack CLI from exiting with an error code if
         * there are errors during compiling - essentially, assets that include errors
         * will not be emitted. If you want your webpack to 'fail', you need to check out
         * the bail option.
         */
        new webpack.NoEmitOnErrorsPlugin(),
        new OpenBrowserPlugin({ url: 'http://localhost:8080' }),
        /**
         * DefinePlugin allows us to define free variables, in any webpack build, you can
         * use it to create separate builds with debug logging or adding global constants!
         * Here, we use it to specify a development build.
         */
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV)
        })
    ]
}