import React from 'react';
import ReactDOM from 'react-dom';
import { Root } from './src/_enter/Root';
import createHistory from 'history/createBrowserHistory'
// import { routerMiddleware } from 'react-router-redux';
import { configureStore } from './src/store/configureStore';
import 'bootstrap/dist/css/bootstrap.min.css';
// import 'bootstrap/dist/css/bootstrap.min.css';
// import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';

// Create a history of your choosing (we're using a browser history in this case)
// const history = createHistory();

// Build the middleware for intercepting and dispatching navigation actions
// const middleware = routerMiddleware(history);

// const { store, persistor } = configureStore(middleware);

const { store, persistor } = configureStore();

ReactDOM.render(
    <Root store={store} persistor={persistor} />,
    document.getElementById('root')
);