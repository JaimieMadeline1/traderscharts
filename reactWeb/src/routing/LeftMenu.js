import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import linkList from '../constants/linkList';
const appConstants = require('../constants/appConstants');
import { Link } from "react-router-dom";
// import { Link, browserHistory } from 'react-router'

class LeftMenu extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <React.Fragment>
                <div>
                    {linkList.map((link) =>
                        <Link to={link.linkTo} key={link.id}>
                            <h3>{link.linkName}</h3>
                        </Link>
                    )}
                </div>
            </React.Fragment>
        );
    }
}

export default LeftMenu;
