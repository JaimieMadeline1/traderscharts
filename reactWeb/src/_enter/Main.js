import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';

import Home from '../screens/Home/Home';

const appConstants = require('../constants/appConstants');

// import createHistory from 'history/createBrowserHistory';

import { PersistGate } from 'redux-persist/integration/react';

// Create a history of your choosing (we're using a browser history in this case)
// const history = createHistory();

class Main extends Component {
    render() {
        return (
            <React.Fragment>
                <PersistGate loading={null} persistor={this.props.persistor}>
                    <React.Fragment>
                        <BrowserRouter>
                            <Home></Home>
                        </BrowserRouter>
                    </React.Fragment>
                </PersistGate>
            </React.Fragment>
        )
    }
}

export default Main;
