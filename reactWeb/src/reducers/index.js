// import { combineReducers } from 'redux';
import { auth } from './auth';
import { loading } from './loading';
import { userState } from './userState';
// const rootReducer = combineReducers({
//     counter: counter, // using just one word syntax also works, but this is more delarative visually
//     auth: auth,
// });

// Don't combine them here, combine them inside configureStore
const rootReducer = {
    auth,
    loading,
    userState
};

export default rootReducer;
