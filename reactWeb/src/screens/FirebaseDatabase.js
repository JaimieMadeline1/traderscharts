import React, { Component, Image } from 'react';
import ReactDOM from 'react-dom';
import firebase from 'firebase';
// Required for side-effects
require("firebase/firestore");

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {
    LOADING_ON,
    LOADING_OFF
} from '../constants/actionTypes';

class FirebaseDatabase extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            listOfClients: []
        };
        this.dataBackup = [];
    }
    componentDidMount() {
        this.props.loadingOn();

        const rootRef = firebase.database().ref().child("users");
        const infoRef = rootRef.child('info');
        const filterData = infoRef.orderByChild("isAccountTypeClient").equalTo(false).limitToLast(100);
        filterData.once('value')
            .then((snapshot) => {
                let dataTemp = [];
                snapshot.forEach((item) => {
                    let initials = item.val().name.match(/\b\w/g) || [];
                    initials = ((initials.shift() || '') + (initials.pop() || '')).toUpperCase();
                    dataTemp.push({
                        uid: item.key,
                        name: item.val().name,
                        nickname: initials,
                        email: item.val().email,
                        portfolioUri: ''
                    });
                });
                this.dataBackup = dataTemp;
                this.loadImages();
                // this.setState({ listOfClients: dataTemp });
            })
            .catch((error) => {
                // this.setState({ status: error.message });
            })
    }
    componentWillUnmount() {
        this.props.loadingOff();
    }
    loadImages() {
        Promise.all(this.dataBackup.map((item) => {
            const rootRefStorage = firebase.storage().ref('Data');
            const userRefStorage = rootRefStorage.child(item.uid);
            const profilePicRefStorage = userRefStorage.child('ProfilePictures');
            const imageRefStorage = profilePicRefStorage.child('profilePic');
            return imageRefStorage.getDownloadURL()
                .then((url) => {
                    item.portfolioUri = url;
                })
                .catch((error) => {
                    // this.setState({ status: error.message });
                });
        })
        )
            .then(() => {
                this.props.loadingOff();
                this.loadImagesFinalDestination();
            })
            .catch(() => {
                this.props.loadingOff();
            });
    }
    loadImagesFinalDestination() {
        this.setState({ listOfClients: this.dataBackup });
    }
    render() {
        return (
            <React.Fragment>
                <h3>Firebase Test.</h3>
                <h4>List all the client's names from firebase database below:</h4>

                <div style={{ marginLeft: '30px' }}>
                    {this.state.listOfClients.length != 0 ?
                        <div>
                            {this.state.listOfClients.map((client) =>
                                <div style={{ border: '1px solid black', padding: '10px', margin: '10px' }} key={client.email}>
                                    {client.portfolioUri != '' ?
                                        <img src={client.portfolioUri} style={{ width: '60px', height: '60px' }} />
                                        :
                                        <div style={{ width: '60px', height: '60px', border: '1px solid black' }}>{client.nickname}</div>
                                    }
                                    <div>Name: {client.name}, Email: {client.email}</div>
                                </div>
                            )}
                        </div>
                        :
                        <div>
                            <h5>There are no clients at the moment.</h5>
                        </div>
                    }
                </div>
            </React.Fragment>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        loadingOn: () => dispatch({ type: LOADING_ON }),
        loadingOff: () => dispatch({ type: LOADING_OFF })
    }
}

export default connect(null, mapDispatchToProps)(FirebaseDatabase);
