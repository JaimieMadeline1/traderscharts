import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import styles from './DescriptionPage.styl';
import { Jumbotron, ListGroup, ListGroupItem, ListGroupItemHeading, ListGroupItemText } from 'reactstrap';
const mockData = require('../../data/projectDescription');

class DescriptionPage extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <React.Fragment>
                <div className={styles.container}>
                    <Jumbotron>
                        <h3>Project Description</h3>
                        <ListGroupItem>
                            <ListGroupItemHeading>Contains</ListGroupItemHeading>
                            {mockData.mockData2.map((item) =>
                                <ListGroupItemText key={item.key}>
                                    - {item.key}
                                </ListGroupItemText>
                            )}
                        </ListGroupItem>
                        <ListGroupItem>
                            <ListGroupItemHeading>Tech Stack</ListGroupItemHeading>
                            {mockData.mockData1.map((item) =>
                                <ListGroupItemText key={item.key}>
                                    - {item.key}
                                </ListGroupItemText>
                            )}
                        </ListGroupItem>
                    </Jumbotron>
                </div>
            </React.Fragment>
        );
    }
}

export default DescriptionPage;